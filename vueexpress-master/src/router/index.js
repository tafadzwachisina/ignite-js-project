import Vue from 'vue'
import Router from 'vue-router'
import Users from '../pages/Users.vue'
import Repos from '../pages/Repos.vue'
import Commits from '../pages/Commits.vue'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'users',
      component: Users
    },
    {
      path: '/users',
      redirect: '/'
    },
    {
      path: '/users/:username/repos',
      name: 'repos',
      component: Repos
    },
    {
      path: '/users/:username/:repo/commits',
      name: 'commits',
      component: Commits
    }
  ]
})
