var fetch = require('node-fetch');
module.exports = function(app) {

  /* Test server is up/down*/
  app.get('/', (req, res) => res.send('Hello World!'))

  /* get search users*/
  app.get('/users', (req, res) =>{
    var url = "https://api.github.com/search/users?q=" + req.query.q;
    fetch(url)
      .then(result => result.json())
      .then(json => res.send(json));
  });

  /* get repos of a user*/
  app.get('/users/:username/repos', (req, res) => {
    var url = "https://api.github.com/users/" + req.params.username + "/repos";
    fetch(url)
      .then(result => result.json())
      .then(json => res.send(json));
  });

  /* get commits of a repo*/
  app.get('/users/:username/repos/:repo/commits', (req, res) => {
    var url = "https://api.github.com/repos/" + req.params.username + "/" + req.params.repo + "/commits";
    fetch(url)
      .then(result => result.json())
      .then(json => res.send(json));
  });
};