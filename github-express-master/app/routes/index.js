const githubRoutes = require('./github_routes');
module.exports = function(app) {
  githubRoutes(app);
  // Other route groups could go here, in the future
};