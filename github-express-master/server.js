const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');
const app = express();
app.use(cors())

const port = 8000;
require('./app/routes')(app, {});

app.listen(port, () => {
  console.log('We are live on ' + port);
});
